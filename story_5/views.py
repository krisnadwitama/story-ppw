from django.shortcuts import render
from django.http import HttpResponseRedirect

from .forms import Portofolio_Form

from .models import Portofolio

def registration(request):
	return render(request, 'Registration.html')

def feedback(request):
	return render(request, 'Feedback.html')

def portofolio(request):
    # if this is a POST request we need to process the form data
    form = Portofolio_Form(request.POST or None)
    response = {}
    if (request.method == 'POST' and form.is_valid()):
    	response['judul'] = request.POST['judul']
    	response['tanggal'] = request.POST['tanggal']
    	response['deskripsi'] = request.POST['deskripsi']
    	response['tempat'] = request.POST['tempat']
    	response['kategori'] = request.POST['kategori']
    	message = Portofolio(judul=response["judul"], tanggal=response["tanggal"],
    		deskripsi=response["deskripsi"], tempat=response["tempat"], kategori=response["kategori"])
    	message.save()
    	return HttpResponseRedirect('Project_List')
    	
    return render(request, 'Portofolio.html', {'portofolio_form': form})

def index(request):
	projects = Portofolio.objects.all().values()
	response = {'projects' : projects}
	return render(request, 'index.html', response)

def project_list(request):
    projects = Portofolio.objects.all().values()
    response = {'projects' : projects}
    return render(request, 'Project_List.html', response)