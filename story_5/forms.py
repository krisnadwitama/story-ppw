from django import forms

class Portofolio_Form(forms.Form):
    judul = forms.CharField(label='Judul Proyek', max_length=100, widget=forms.TextInput(attrs={'size':38}))
    tanggal = forms.CharField(label='Tanggal mulai - tanggal selesai', max_length=100, widget=forms.TextInput(attrs={'size':38}))
    deskripsi = forms.CharField(label='Deskripsi Singkat Proyek', max_length=100, widget=forms.Textarea(attrs={'rows':3, 'cols':40}))
    tempat = forms.CharField(label='Tempat', max_length=100, widget=forms.TextInput(attrs={'size':38}))
    kategori = forms.CharField(label='Kategori Proyek', max_length=100, widget=forms.TextInput(attrs={'size':38}))