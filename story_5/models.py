from django.db import models
from django.utils import timezone
from datetime import datetime, date

class Portofolio(models.Model):
	judul = models.CharField(max_length=100)
	tanggal = models.CharField(max_length=100)
	deskripsi = models.CharField(max_length=100)
	tempat = models.CharField(max_length=100)
	kategori = models.CharField(max_length=100)